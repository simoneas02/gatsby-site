import * as React from 'react'
import Layout from '../components/layout/layout'

const IndexPage = () => (
  <Layout pageTitle="Home">
    <p>Welcome to my website</p>
  </Layout>
)

export default IndexPage
