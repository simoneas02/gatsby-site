import * as React from 'react'
import Layout from '../../components/layout/layout'

const AboutPage = () => (
  <Layout pageTitle="About Me">
    <p>Hi there! I'm Simone Amorim and I'm a software developer.</p>
  </Layout>
)

export default AboutPage
